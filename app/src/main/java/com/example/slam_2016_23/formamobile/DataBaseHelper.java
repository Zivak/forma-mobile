package com.example.slam_2016_23.formamobile;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;

/**
 * Created by slam-2016-23 on 04/02/2016.
 */
public class DataBaseHelper extends SQLiteOpenHelper
{
    //The Android's default system path of your application database.
    private static String DB_PATH = "/data/data/com.example.slam_2016_23.formamobile/databases/";
    private static String DB_NAME = "forma";
    private SQLiteDatabase myDataBase;
    private final Context myContext;

    //Nom de l'activité qui appelle cette classe.
    private static final String TAG = MainActivity.class.getSimpleName();


    /**
     * Constructor
     * Takes and keeps a reference of the passed context in order to access the application assets and ressources;
     * @param context
     */
    public DataBaseHelper(Context context) {
        super(context, DB_NAME, null, 1);
        this.myContext = context;
    }

    /**
     * Creates an empty database on the system and rewrites it with your own database.
     * @throws IOException
     */
    public void createDatabase() throws IOException {
        boolean dbExist = checkDataBase();

        if(dbExist) {
            //do nothing - database already exist
            Log.d(TAG,"La BDD existe déjà, pas de création.");
        } else {
            Log.d(TAG, "La BDD n'existe pas, tentative de création.");
            getReadableDatabase();

            try {
                copyDataBase();
            } catch(IOException e) {
                throw new Error("Error copying database");
            }
        }

        this.close();
    }

    /**
     * Check if the database already exist to avoid re-copying the file each time you open the application.
     * @return true if it exists, false if it doesn't
     */
    private boolean checkDataBase() {
        SQLiteDatabase checkDB = null;

        try {
            String myPath = DB_PATH + DB_NAME;
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
        } catch(SQLiteException e) {
            //database doesn't exist yet.
        }

        if(checkDB != null){
            checkDB.close();
        }

        return checkDB != null;
    }

    /**
     * Copies your database from local assets-folder to the just created empty database in the system folder,
     * from where it can be accessed and handled;
     * This is done by transferring bytestream.
     * @throws IOException
     */
    private void copyDataBase() throws IOException {
        Log.d(TAG,"copy");

        //Open your local db as the input stream
        InputStream myInput = myContext.getAssets().open(DB_NAME);

        //Path to the just created empty db.
        String outFileName = DB_PATH + DB_NAME;

        Log.d(TAG,"Output: " + outFileName);

        //Open the empty db as the output stream.
        OutputStream myOutput = new FileOutputStream(outFileName);

        //transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while((length = myInput.read(buffer)) > 0){
            myOutput.write(buffer, 0, length);
        }

        //Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    public void openDataBase() throws SQLException {
            //Open the database
            String myPath = DB_PATH + DB_NAME;
            myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
    }

    @Override
    public synchronized  void close() {
        if(myDataBase != null)
            myDataBase.close();

        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){

    }

    //Helper methods.
    public boolean login(String login, String password){
        boolean success = false;

        Cursor mCursor =   myDataBase.query("PARTICIPANT", new String[]{"LOGIN_PART", "MDP_PART"}, "LOGIN_PART = ? AND MDP_PART = ?", new String[]{login, password}, null, null, null);

        //Si il y a un jeu de résultats.
        if(mCursor.getCount() > 0){
            success = true; //La connexion est réussie.
        }

        //Fermeture du curseur.
        mCursor.close();

        return success;
    }
}
